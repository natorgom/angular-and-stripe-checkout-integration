# Building an Angular 11 Storefront with Stripe Checkout and Firebase Functions


# Overview
1. Create the Firebase app
1. Create the Angular app
1. Connect Firebase to Angular
1. Add Stripe Checkout to the Angular app
1. Build for production


# Create your Firebase App
The Firebase website changes fairly often, so the exact wording below might change a little.

We're going to use Cloud Firestore to build a NoSQL database to store details about our products.

* firebase.google.com > (login) > "Go to console" > "Add project" > name: "store" >
    Continue > (we're not going to enable Google Analytics) > "Create project" 
    
* when the project is created, you'll be taken to its dashboard, click the small "</>" Web logo > 
    enter "store" for "My web app" name > (no need to set up Firebase hosting) > "Register app"

* you'll be taken to a page with Firebase SDK code. copy this code! we'll work with the `firebaseConfig` variable in a later step when we connect our Firebase and Angular apps

## Set up the Cloud Firestore database
* in the left column, browse to Build > Cloud Firestore > click "Create database" > "Start in test mode" > "Next" > (choose a location close to you) > "Enable"

Here are the geographic locations: https://firebase.google.com/docs/firestore/locations

## Cloud Firestore test data
* add a few items in a collection of `clothes`

* documents inside the `clothes` collection contain the fields `{name<string>, price<number>, id<string>, image<string>}`

## Set up the Firebase Function

* normally, we'd build the strorefront first, but to limit the amount of jumping around,
we're going to do all Firebase work at once

* we're going to build a folder seperate from our Angular app

> mkdir firebaseFunctions

> cd firebaseFunctions

* in your browser, in the left column, browse to Build > Functions > click "Upgrade project" > "Set a Budget" > (I'm using $2) > Close

* "Get Started" > (copy the `npm` command on screen)

* Firebase Functions require a command-line interface, so we'll install the CLI

> npm install -g firebase-tools

> npm install firebase-functions@latest firebase-admin@latest --save

> firebase login

* go through the login steps & paste the verification code

> firebase init

* use you keyboard arrows to select `Functions` > (hit space) > Enter 
* select "Use an existing project" > "store-####"
* language? Javascript
* linter? No
* install dependencies? Yes

> firebase use --add

* choose `store-####`
* alias? "store"

open `firebaseFunctions/functions/index.js` and paste my code

> cd functions

> npm install stripe

> cd ..
             
## Test the code locally

It takes about 1 minute to upload and deploy code to Firebase's serverless
environment, so we're going to build our code locally first and only deploy
once it's debugged and ready

> firebase emulators:start

> firebase serve

* the terminal will tell you which port the emulator is using (usually port 5000)


# Create your Angular App

## Make sure your coding environment is up to date
> ng version            
* make sure you're at least using Angular 11

> npm -v                
* make sure you're at least using npm version 7

## Create the Angular project

* exit the `FirebaseFunctions` folder, and go to its parent folder
> cd ..

> npm install -g @angular/cli

> ng new storefront
* Enforce stricter type checking? No
* Add Angular routing? Yes
* Which stylesheet? CSS

> cd storefront

## Add the Firebase and Angular Material libraries

> ng add @angular/fire
* Please select a project: (the newly created Firebase "store-####" app)

> ng add @angular/material
* Choose a prebuilt theme: (any)
* Typography: Yes
* Browser animations: (yes or no; I'll be choosing yes)

Note: you can choose "No", but even if you choose "Yes" and end up
not using it, when you build your app for production, all unused libraries will be removed

## Start the Server
> ng serve --port 4205 --open

## Remove boilerplate code
* in `storefront/src/app/app.component.html`, only keep the `<router-outlet></router-outlet>` tag at the very bottom of the file

## Add components
> ng generate component home

## Link the pages (Set up page routing)
* open `storefront/src/app/app-routing.module.ts`
    * import the component
    * add the component to the `routes:[...]` array
    

# Connect Firebase to Angular

## Use the Firebase App credentials
* open `storefront/src/environments/environment.ts` and `environment.prod.ts`
* copy the earlier snippet of code when we created the Firebase app to both `environment` files, renaming `firebaseConfig` to `firebase`
* in case you're using `git`, you won't want to publish this information, so open your `.gitignore` file, make sure to include the line `/src/environments/**`
* note: since these files are not part of my GitLab project, here's what my `environment.ts` file generally looks like:

```
export const environment = {
    production: false,
    firebase: {
        apiKey: "MY_DATA_HERE",
        authDomain: "MY_DATA_HERE",
        projectId: "MY_DATA_HERE",
        storageBucket: "MY_DATA_HERE",
        messagingSenderId: "MY_DATA_HERE",
        appId: "MY_DATA_HERE"
    }
};
```

* keep in mind that Firebase often changes what info is put inside the `firebaseConfig = {...}` variable, so if Firebase supplies you with slightly different info, that's fine

## Add basic navigation and style to each component
* open `storefront/src/app/app.module.ts`
    * import the additional modules that we'll be using (see my code)
    * add these modules to the `imports:[...]` array
    * this lets us use Angular Material in our components and it imports our Firebase environment variables

* open `storefront/src/app/home/home.component.ts`, `.html`, and `.css`
    * import the code


# Add Stripe Checkout to Angular

Good tutorial: https://stripe.com/docs/payments/checkout

* In a terminal, make sure you're in the `storefront` folder
> npm install --save stripe

* open `storefront/src/index.html` and between the `<head></head>` tags, add `<script src="https://js.stripe.com/v3/"></script>`

* open `storefront/src/environments/environment.ts` and `environment.prod.ts`
* copy your stripe Publishable Key in
* example:
```
    stripe: {
        key: 'pk_test_MY_KEY'
    }
```
    * after you log in to Stripe, your keys can be found here: https://stripe.com/docs/payments/accept-a-payment?integration=checkout



# Build for production 

## Build the Firebase Function for production

* change the paths for `success` and `cancel` in `firebaseFunctions/functions/index.js` to point to your production domain

> cd ../FirebaseFunctions

> firebase deploy
* this will take about a minute to complete

## Build the Angular storefront for production

* open `storefront\src\app\home.component.ts` and comment out the code in the 
constructor that reads `afFun.useEmulator("localhost", 5001);`

> ng build --prod

* building for production will take a little time, but all of the unused libraries will be removed!

* you can take the contents of your `storefront/dist/storefront` folder and upload it to the `html` folder of your website


