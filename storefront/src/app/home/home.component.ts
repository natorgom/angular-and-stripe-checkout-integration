import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

declare var Stripe;

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    clothes: Observable<any[]>;                     // all items in the Cloud Firestore "clothes" collection
    stripeStatus: string;                           // {'', 'cancel', 'success'}

    constructor(
        private firestore: AngularFirestore,
        private afFun: AngularFireFunctions,
        private activatedRoute: ActivatedRoute,
        private router: Router) {

        afFun.useEmulator("localhost", 5001);
        this.clothes = null;
        this.stripeStatus = '';
    }

    ngOnInit(): void {
        // this.clothes = this.firestore.collection('clothes').valueChanges();                                      // get all items
        this.clothes = this.firestore.collection('clothes', ref => ref.where('name', '!=', null)).valueChanges();   // get certain items

        this.stripeStatus = this.getStripeStatus();                                         // read URL for "/home?action="
    }

    getStripeStatus(): string {
        let action = this.activatedRoute.snapshot.queryParamMap.get('action');              // ex: '/home?action=success'
        console.log('action = ', action);
        if (action && action == 'cancel' || action == 'success')
            return action;
        return '';
    }

    checkoutFirebase(productId: string): void {
        console.log('checking out with item id: ' + productId);

        var stripe = Stripe(environment.stripe.key);

        this.afFun.httpsCallable("stripeCheckout")({ id: productId })
            .subscribe(result => {
                console.log({ result });

                stripe.redirectToCheckout({
                    sessionId: result,
                }).then(function (result) {
                    console.log(result.error.message);
                });
            });
    }

    reloadHome(): void {
        this.router.navigate(['/home'])
            .then(() => {
                window.location.reload();
            });
    }
}
